let collection = [];
// Write the queue functions below.

function enqueue(element) {

    // collection.push(element);
    // return collection;

    collection[collection.length] = element
    return collection
}


function print() {
    return collection;
}


function dequeue() {
    let nameCollection = [];
    for (let i = 0; i < collection.length; i++) {
        if (i > 0){
            nameCollection[i-1] = collection[i];
        }
    }
    collection = nameCollection;
    return collection;


}

function front() {
    return collection[0];

}

function size() {

    return collection.length;

}

function isEmpty() {

    if(collection.length > 0){
        return false;
    } else {
        return true;
    }

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
